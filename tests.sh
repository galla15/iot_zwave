#!/bin/bash

red='\033[0;31m'
green='\033[0;32m'
reset='\033[0m'

_continue(){
    echo "Press enter to continue"
    while read -r -n1 key; do
        if [[ $key != $'\e' ]]; then
            break;
        else
            exit 0
        fi
    done
}

compare(){
    if [ "$1" = "$2" ]; then
        echo -e "${green}Success${reset} : $2"
    else
        echo -e "${red}Failed : $2 ${reset}"
        exit 1
    fi

    _continue
    
}


rbpi_ip=192.168.1.125
cli_path=.
zwcli="${cli_path}/post_client.py -u http://${rbpi_ip}"

test_num=1

###1
echo "### Test ${test_num} inclusion/exclusion ###"

echo "### Inclusion timeout"
result="InclusionError -- Timeout occurred. No node found after 20 s"
output=$($zwcli node add)
compare "${result}" "${output}"

echo "### Exclusion timeout"
result="ExclusionError -- Timeout occurred. No node removed after 20 s"
output=$($zwcli node remove)
compare "${result}" "${output}"

test_num=$((test_num + 1))

###2
echo "### Test ${test_num} add"
echo "### add sensor"
$zwcli node add
_continue

echo "### add dimmer"
$zwcli node add
_continue

$zwcli network nodes
$zwcli network info

_continue

test_num=$((test_num + 1))

###3
echo "### Test ${test_num} probe/set parameter"

echo "### bad node number"
result="QueryFail -- Node not found"
output=$($zwcli node parameter -n 255 -i 111)
compare "${result}" "${output}"

echo "### not a sensor"
result="null"
output=$($zwcli node parameter -n 3 -i 111)
compare "${result}" "${output}"

echo "### read param 111"
result="3600"
output=$($zwcli node parameter -n 2 -i 111)
compare "${result}" "${output}"

echo "### set param 111 to 480"
$zwcli node set_parameter -d '{"node_id":2, "value":480, "index":111, "size":4}'
_continue

echo "### Check param"
result="480"
output=$($zwcli node parameter -n 2 -i 111)
compare "${result}" "${output}"

test_num=$((test_num + 1))

###4
echo "### Test ${test_num} set node info field"

echo "### read location"
$zwcli node location -n 3
_continue

echo "### set location"
$zwcli node set_location -d '{"node_id":3, "value":"lab"}'
_continue

echo "### Check location"
result="\"lab\""
output=$($zwcli node location -n 3)
compare "${result}" "${output}"

test_num=$((test_num + 1))

###5
echo "### Test ${test_num} list neighbours"
$zwcli node neighbours -n 3
_continue

test_num=$((test_num + 1))

###6
echo "### Test ${test_num} Sensor"

echo "### Readings"
$zwcli sensor readings -n 2
_continue

echo "### Humidity"
$zwcli sensor humidity -n 2
_continue

echo "### Battery level"
$zwcli sensor battery -n 2
_continue

test_num=$((test_num + 1))

###7
echo "### Test ${test_num} Dimmer"

echo "### Not a dimmer"
result="QueryFail -- Not a dimmer"
output=$($zwcli dimmer level -n 2)
compare "${result}" "${output}"

echo "### Get level"
$zwcli dimmer level -n 3
_continue

echo "### Set level"
result="99"
output=$($zwcli dimmer set_level -d '{"node_id":3, "value": 50}')
compare "${result}" "${output}"

echo "### Check level"
$zwcli dimmer level -n 3
_continue

test_num=$((test_num + 1))

###clean up
echo "### Clean-up ###"
echo "### Remove sensor"
$zwcli node remove
_continue

echo "### Remove dimmer"
$zwcli node remove
_continue

echo "### Hard reset"
$zwcli network hard_reset

$zwcli network info
