#!/bin/bash

rbpi_ip=192.168.1.125
cli_path=.
zwcli="${cli_path}/post_client.py -u http://${rbpi_ip}"

echo "###Removing existing nodes"

$zwcli node remove
$zwcli node remove
$zwcli network nodes

echo "### Hard reset"
$zwcli network hard_reset